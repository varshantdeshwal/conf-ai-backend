const { RESTDataSource } = require('apollo-datasource-rest');
const { getDocument, addDocument, deleteDocument } = require('../database/util');
const { roomSchema } = require('../database/schemas');
const { ROOM } = require('../database/constants');
class RoomAPI extends RESTDataSource {
    constructor() {
        super();
    }

    async addRoom(body) {
        console.log('add room called');

        const data = await getDocument({ collection: ROOM, schema: roomSchema, query: { name: body.name } });
        if (data.length) {

            return { message: "Room already exists." }

        }
        let query = {
            name: body.name,
            seating_capacity: body.seatingCapacity
        };
        const result = await addDocument({ collection: ROOM, schema: roomSchema, query });
        return { room: result, message: "success" }
    }
    async deleteRoom({ roomId }) {
        console.log('delete room called');

        const result = await deleteDocument({ collection: ROOM, schema: roomSchema, query: { _id: roomId } });

        if (result)
            return { room: result, message: "success" };
    }
    async fetchAllRooms() {
        console.log('fetch all rooms called');

        let query = {};
        const result = await getDocument({ collection: ROOM, schema: roomSchema, query });

        return result;
    }


}

module.exports = RoomAPI;