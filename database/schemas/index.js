const userSchema =require('./userSchema');

module.exports = {
    userSchema,
    roomSchema: require('./roomSchema'),
    bookingSchema: require('./bookingSchema'),
}