const mongoose = require('mongoose');

const Schema =mongoose.Schema;
const roomSchema = Schema({
  name: { type: String, required: true },
  seating_capacity: { type: Number, required: true },
});

module.exports = roomSchema;