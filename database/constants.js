const TOKEN_SECRET_KEY = 'decipher';
const USER = 'user';
const ROOM = 'room';
const BOOKING = 'booking';

module.exports = {
    TOKEN_SECRET_KEY,
    USER,
    ROOM,
    BOOKING
}