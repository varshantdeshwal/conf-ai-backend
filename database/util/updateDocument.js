const { getModel } = require('../model');

const updateDocument = async ({ collection, schema, query, update = {} }) => {
    const model = getModel({ collection, schema });
    try {
        return await model.findOneAndUpdate(query, update);
    }
    catch (err) {
        throw (err);
    }
};

module.exports = updateDocument;