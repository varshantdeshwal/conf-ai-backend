const { getModel } = require('../model');
const addDocument = async ({ collection, schema, query }) => {
    const model = getModel({ collection, schema });
    try {
        return await model.create(query);
    }
    catch (err) {
        throw (err);
    }
};
module.exports = addDocument;