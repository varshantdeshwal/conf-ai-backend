const { getModel } = require('../model');

const deleteDocument = async ({ collection, schema, query }) => {
    const model = getModel({ collection, schema });
    try {
        return await model.remove(query);
    }
    catch (err) {
        throw (err);
    }
};

module.exports = deleteDocument;