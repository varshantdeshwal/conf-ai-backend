const addDocument =require('./addDocument');
const getDocument = require('./getDocument');
const updateDocument = require('./updateDocument');
const deleteDocument = require('./deleteDocument');

module.exports ={
    addDocument,
    getDocument,
    updateDocument,
    deleteDocument
}