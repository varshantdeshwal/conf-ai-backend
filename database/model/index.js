var mongoose = require("mongoose");
const getModel = ({collection, schema}) => {
    return mongoose.model(collection, schema);
};

module.exports = {
    getModel
}