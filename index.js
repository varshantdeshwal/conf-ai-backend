const { ApolloServer } = require('apollo-server');
const typeDefs = require('./schema');
const UserAPI = require('./dataSources/user')
const RoomAPI = require('./dataSources/room')
const BookingAPI = require('./dataSources/booking');
const resolvers = require('./resolver');
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");

const TOKEN_SECRET_KEY = 'decipher';

const getContext = ({ req }) => {
  const authHeader = req.headers['authorization'] || '';
  const token = authHeader.split(" ")[1];
  let user;
  try {
    if(token)
    user = jwt.verify(token, TOKEN_SECRET_KEY);
  }
  catch (err) {
    throw(err);
  }
  return {
    user
  }
};

mongoose.connect("mongodb://varshant:Farmtrac14@ds345028.mlab.com:45028/ai-conf", { poolSize: 10 });
var db = mongoose.connection;
db.on("error", function () { console.log('Mongo error'); });
db.on('connected', function () { console.log('MongoDB Connected'); });
db.on('disconnected', function () { console.log('MongoDB Disconnected'); });

// If the Node process ends, close the Mongoose connection 
process.on('SIGINT', function () {
  mongoose.connection.close(function () {
    console.log('Mongoose default connection disconnected through app termination');
    process.exit(0);
  });
});

const server = new ApolloServer({
  typeDefs,
  resolvers,
  dataSources: () => ({ userAPI: new UserAPI(), roomAPI: new RoomAPI(), bookingAPI: new BookingAPI() }),
  context: getContext
});
server.listen({port: process.env.PORT || 4000}).then(({ url }) => {
  console.log(`🚀 Server ready at ${url}`);
});