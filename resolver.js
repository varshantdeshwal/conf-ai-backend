module.exports = {
    Query: {
        rooms: async (_, __, { dataSources, user }) => {
            if(!user) throw new Error("You are not logged in to access this information ");

            const response = await dataSources.roomAPI.fetchAllRooms();
            return response
        },
        booking: async (_, args, { dataSources, user }) => {
            if(!user) throw new Error("You are not logged in to access this information ");
            const response = await dataSources.bookingAPI.fetchBooking(args);
            return response
        },
        roomBookings: async (_, args, { dataSources, user }) => {
            if(!user) throw new Error("You are not logged in to access this information ");
            const response = await dataSources.bookingAPI.fetchRoomBookings(args);
            return response
        },
        userBookings: async (_, args, { dataSources, user }) => {
            if(!user) throw new Error("You are not logged in to access this information ");
            const response = await dataSources.bookingAPI.fetchUserBookings(args);
            return response
        },
    },
    Mutation: {
        signup: async (_, args, { dataSources }) => {
            const response = await dataSources.userAPI.signup(args);
            console.log("----------------",response);
            return response;
        },
        signin: async (_, args, { dataSources }) => {
            const response = await dataSources.userAPI.signin(args);
            return response;
        },

        addRoom: async (_, args, { dataSources, user }) => {
            if(!user) throw new Error("You are not logged in to access this information ");
            const response = await dataSources.roomAPI.addRoom(args);
            return response;
        },

        deleteRoom: async (_, args, { dataSources, user }) => {
            if(!user) throw new Error("You are not logged in to access this information ");
            const response = await dataSources.roomAPI.deleteRoom(args);
            return response;
        },

        addBooking: async (_, args, {dataSources, user}) => {
            if(!user) throw new Error("You are not logged in to access this information ");
            const response = await dataSources.bookingAPI.addBooking(args);
            return response;
        },
        deleteBooking: async (_, args, { dataSources, user }) => {
            if(!user) throw new Error("You are not logged in to access this information ");
            const response = await dataSources.bookingAPI.deleteBooking(args);
            return response;
        },
        
    }
}